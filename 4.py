import json
import pandas as pd

# Сюди вставляємо шлях до json файлу який ми попередньо завантажили з аналітики мера
with open('/Users/goncharovvitalii/Desktop/school.json', 'r') as f:
    data = json.load(f)


id_list = []
name_list = []
teachers_list = []
class1_4_list = []
class5_9_list = []
class10_11_list = []
students_list = []

for school in data:
    if 'ID' in school and 'Name' in school and 'Teachers' in school and 'Parallels' in school and 'Schoolboys' in school:
        id_list.append(school['ID'])
        name_list.append(school['Name'])
        teachers_list.append(school['Teachers']['All'])
        class1_4_list.append(school['Parallels'].get('1', {'All': 0})['All'] + school['Parallels'].get('2', {'All': 0})['All'] +
                             school['Parallels'].get('3', {'All': 0})['All'] + school['Parallels'].get('4', {'All': 0})['All'])
        class5_9_list.append(school['Parallels'].get('5', {'All': 0})['All'] + school['Parallels'].get('6', {'All': 0})['All'] +
                             school['Parallels'].get('7', {'All': 0})['All'] + school['Parallels'].get('8', {'All': 0})['All'] +
                             school['Parallels'].get('9', {'All': 0})['All'])
        class10_11_list.append(school['Parallels'].get('10', {'All': 0})['All'] + school['Parallels'].get('11', {'All': 0})['All'])
        students_list.append(school['Schoolboys']['All'])


df = pd.DataFrame({
    'id': id_list,
    'Повна назва': name_list,
    'Кількість вичтелів': teachers_list,
    'К-сть унів 1-4 класи': class1_4_list,
    'К-сть унів 5-9 класи': class5_9_list,
    'К-сть унів 10-11 класи': class10_11_list,
    'К-сть унів всього': students_list
})


file_path = input("Enter the path to the Excel file: ")
df.to_excel(file_path, index=False)
